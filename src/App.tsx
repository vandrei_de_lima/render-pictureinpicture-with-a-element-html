import "./App.css";
//@ts-ignore
import Whammy from "react-whammy";
import html2canvas from "html2canvas";
import { useState } from "react";

function App() {
  const [input, setInput] = useState("");
  const [tasks, setTasks] = useState<any[]>([]);

  function addTask(): void {
    if (input) {
      setTasks([...tasks, { text: input, completed: false }]);

      setInput("");
    } else {
      alert("Campo vazio");
    }
  }

  function removeTask(index: number): void {
    tasks.splice(index, 1);
    setTasks([...tasks]);

  }

  function completeTask(index: number): void {
    tasks[index].completed = true;

    setTasks([...tasks]);

    openPopoup()
  }

  function openPopoup(): void {
    const elemento: HTMLElement = document.getElementById(
      "elemento"
    ) as HTMLElement;
    const elementoVideo: HTMLVideoElement = document.getElementById(
      "elementoVideo"
    ) as HTMLVideoElement;

      let postIt:any = elemento.cloneNode(true)

      document.body.appendChild(postIt);

      postIt.classList.remove("shadow-postIt");
      postIt.classList.add("hide--postIt");

    html2canvas(postIt).then((canvas) => {
      document.body.appendChild(postIt);
      const encoder = new Whammy.Video(1); //fps

      encoder.add(canvas);

      encoder.compile(false, (output: any) => {
        const video = URL.createObjectURL(output);

        elementoVideo.src = video;

        setTimeout(async () => {
          try {
            await elementoVideo.requestPictureInPicture();
          } catch (error) {
            console.log("Problema ao abrir o popoup");
          }
        }, 500);
      });
    });
  }

  function IsCompleted(props: { isCompleted: boolean; index: number }): any {
    const { isCompleted, index } = props;
    if (isCompleted) {
      return null;
    }
    return (
      <span className="cursor-pointer" onClick={() => completeTask(index)}>
        Pronto
      </span>
    );
  }

  function CheckCompletTask(props: { item: any }): any {
    const { item } = props;

    if (item.completed) {
      return (
        <li>
          <s>{item.text}</s>
        </li>
      );
    }
    return <li>{item.text}</li>;
  }

  return (
    <div className="App">
      <h3>Post it temporário</h3>
      <div className="card">
        <div className="gap-10">
          <input
            type={"text"}
            id="descriptionTask"
            placeholder="Descrição tarefa"
            value={input}
            onInput={(e: any) => setInput(e.target.value)}
          />
          <button onClick={() => addTask()}>Adicionar</button>
        </div>
        <div>
          <h4>Lista de tarefas</h4>
          <ul>
            {tasks.map((item: any, index: number) => (
              <li className="task-list">
                {item.text}
                <strong className="gap-10">
                  <IsCompleted isCompleted={item.completed} index={index} />
                  <span
                    className="cursor-pointer"
                    onClick={() => removeTask(index)}
                  >
                    Remover
                  </span>
                </strong>
              </li>
            ))}
          </ul>
        </div>
        <h4>Post it</h4>
        <div className="posrIt shadow-postIt" id="elemento">
          <ul>
            {tasks.map((item: any, index: number) => (
              <CheckCompletTask item={item} />
            ))}
          </ul>
        </div>
        <button className="mt-2" onClick={() => openPopoup()}>
          Colar post it
        </button>
        <div className="no--visible">
          <video height={150} width={300} id="elementoVideo" controls></video>
        </div>
      </div>
    </div>
  );
}

export default App;
